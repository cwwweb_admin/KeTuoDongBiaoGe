function remember(key, value, minutes) {
    let expired = -1;
    if (minutes) {
        expired = new Date().getTime() + minutes * 60000;
    }
    localforage.setItem(key, { expired, value });
    return value;
}

function getRemember(key, callback) {
    localforage.getItem(key, function(err, data) {
        if (err) {
            console.error('Oh noes!');
        } else {
            if(data) {
                callback && callback(data.value);
            } else {
                callback(null)
            }
        }
    });
}
// 取出并删除值
function flash(key, defVal) {
    let value = get(key, defVal);
    if (!value) {
        forget(key);
    }
    return value;
}
// 删除值
function forget(key) {
    if (typeof key == 'string') {
        localforage.removeItem(key);
    } else {
        console.warn('[CacheUtil::forget] Key only allow Array or String.');
    }
}

function clear() {
    localforage.clear();
}